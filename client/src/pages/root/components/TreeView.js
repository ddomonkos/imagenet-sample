import React from 'react';
import './TreeView.scss';

function compareNames(a, b) {
  const aName = a.name.toLowerCase();
  const bName = b.name.toLowerCase();
  return aName < bName ? -1 : (aName > bName ? 1 : 0);
}

function renderNode(node, onClick, nestLevel) {
  let content;
  const {id, name, size, children, match} = node;
  if (children && children.length > 0) {
    content = <TreeView root={node} onClick={onClick} nestLevel={nestLevel} />;
  } else {
    content = (
      <div className={`node ${size > 0 ? 'clickable' : ''} ${match ? 'match' : ''}`}
        onClick={size > 0 ? () => onClick(id, []) : undefined}>
        <span className='name'>{name}</span>
        {size > 0 && <span className='size'>{size}</span>}
      </div>
    );
  }

  return (
    <li key={id} title={name}>
      {content}
    </li>
  );
}

class TreeView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: true
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(id, path) {
    const {root, onClick} = this.props;
    if (onClick) {
      onClick(id, [root.id].concat(path));
    }
  }

  render() {
    const {root, nestLevel = 0} = this.props;
    return (
      <div className={`tree-view nest-${nestLevel}`}>
        <div className={`root clickable ${root.match ? 'match' : ''}`}
          onClick={() => this.setState({expanded: !this.state.expanded})}>
          <span className='name'>{root.name}</span>
          <span className='size'>{root.size}</span>
        </div>

        <ul className={this.state.expanded ? 'expanded' : ''}>
          {root.children
            .sort(compareNames)
            .map(child => (
              renderNode(child, this.handleClick, nestLevel + 1)
            ))}
        </ul>
      </div>
    );
  }
}

export default TreeView;
