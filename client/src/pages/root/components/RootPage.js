import React from 'react';
import api from 'core/api';
import TreeView from './TreeView';
import { Subject } from 'rxjs/Subject';
import './RootPage.scss';

export default class RootPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.searchRequest$ = new Subject();
    this.unmount$ = new Subject();

    this.searchRequest$
      .debounceTime(300)
      .takeUntil(this.unmount$)
      .subscribe(val => {
        if (val.length > 0) {
          this.search(val);
        } else {
          this.setState({searchResults: undefined});
        }
      });

    this.handleChildClick = this.handleChildClick.bind(this);
    this.handleNavigationClick = this.handleNavigationClick.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  componentWillMount() {
    this.fetchNode();
  }

  componentWillUnmount() {
    this.unmount$.next();
  }

  handleChildClick(id, path) {
    this.fetchNode(id, path);
  }

  handleSearchChange(ev) {
    this.searchRequest$.next(ev.target.value);
  }

  search(value) {
    api.get('/', {query: value}).then(res => {
      this.setState({searchResults: res.data});
    });
  }

  handleNavigationClick(id) {
    const {path} = this.state;
    const index = path.indexOf(path.find(item => item.id === id));
    this.setState({path: path.slice(0, index)});
    this.fetchNode(id);
  }

  fetchNode(id, path) {
    api.get(id == null ? '/' : `/${id}`).then(res => {
      if (path == null) {
        this.setState({root: res.data});
      } else {
        const node = path
          .slice(1)
          .concat([id])
          .reduce((node, id) => (
            node.children.find(child => child.id === id)
          ), this.state.root);
        node.children = res.data.children;
        this.setState({root: Object.assign({}, this.state.root)});
      }
    });
  }

  render() {
    const {root, searchResults} = this.state;
    return (
      <div className='root-page'>
        <input className='search' type='text' onChange={this.handleSearchChange} />

        {root && !searchResults && <TreeView root={root} onClick={this.handleChildClick} />}
        {searchResults && searchResults.map(root => <TreeView root={root} key={root.id} />)}
        {searchResults && searchResults.length === 0 && <div className='empty'>No results</div>}
      </div>
    );
  }
}
