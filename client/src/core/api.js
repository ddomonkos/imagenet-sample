import axios from 'axios';

function getUrl(path) {
  return process.env.API_ENDPOINT + path;
}

export default {
  get: function(path, params) {
    return axios.get(getUrl(path), {params});
  }
};
