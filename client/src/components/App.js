import React from 'react';
import { RootPage } from 'pages/root';
import './App.scss';

export default () => (
  <RootPage />
);
