const axios = require('axios');
const xml2js = require('xml2js');
const db = require('@/core/db');
const Promise = require('promise');
const SEPARATOR = require('@/core/name').SEPARATOR;


class Scraper {
  constructor(workersLimit = 1) {
    this.queue = [];
    this.pendingPromises = [];
    this.workersLimit = workersLimit;
  }

  async scrape(id) {
    await clearDb();
    this.queue.push({id, category: []});
    const promises = Array.from(Array(this.workersLimit)).map(() => this::runWorker());
    await new Promise.all(promises);
  }
}


async function clearDb() {
  return await db.query('DELETE FROM imagenet');
}


async function runWorker() {
  while (true) {
    while (this.queue.length == 0 && this.pendingPromises.length > 0) {
      await this.pendingPromises[0];
    }
    const task = this.queue.shift();
    if (!task) {
      break;
    }

    const promise = this::scrape(task.id)
      .then(results => {
        if (task.category.length == 0) { // this is the tree root - store it
          storeNode(results.node);
        }
        const subcategory = task.category.concat([results.node.name]);
        results.children.forEach(child => {
          storeNode(child, subcategory, task.id);
          if (child.size > 0) { // stop if no children
            this.queue.push({id: child.id, category: subcategory});
          }
        });
      })
      .catch(() => {
        // console.error(err);
        this.queue.push(task);
        console.error(`#${task.id} failed. Pushed back onto the queue`);
      });

    this.pendingPromises.push(promise);
    await promise;
    this.pendingPromises.splice(this.pendingPromises.indexOf(promise), 1);
  }
}


// download and store nodes
async function scrape(id) {
  const response = await axios.get(`http://imagenet.stanford.edu/python/tree.py/SubtreeXML?rootid=${id}`);
  const xml = await this::parseXml(response.data);
  const node = this::extractNodeData(xml.synset);

  let children = [];

  if (xml.synset.synset) {
    children = xml.synset.synset.map(extractNodeData);
  }

  return {node, children};
}


// save it into the database
//
async function storeNode(node, category = [], parentId = null) {
  const name = category.concat([node.name]).join(SEPARATOR);
  return await db.query(
    'INSERT INTO imagenet (id, name, size, parent_id) VALUES ($1, $2, $3, $4)',
    [node.id, name, node.size, parentId]
  );
}


// wrap the parser so that it is rxjs-based
//
async function parseXml(str) {
  return await new Promise((resolve, reject) => {
    xml2js.parseString(str, (err, xml) => {
      if (err) {
        reject(err);
      } else {
        resolve(xml);
      }
    });
  });
}


// prettify the result object
//
function extractNodeData(xmlNode) {
  const size = parseInt(xmlNode.$.subtree_size, 10);
  return {
    id: xmlNode.$.synsetid,
    name: xmlNode.$.words,
    size: isNaN(size) ? undefined : size - 1
  };
}




module.exports = Scraper;
