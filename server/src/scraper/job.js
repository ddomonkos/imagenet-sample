const Scraper = require('./scraper');

const ROOT_ID = 82127;

const scraper = new Scraper(10);

scraper
  .scrape(ROOT_ID)
  .then(() => console.log('done'))
  .catch(err => console.log('error', err));
