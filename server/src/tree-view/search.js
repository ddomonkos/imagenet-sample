const db = require('@/core/db');
const isolateName = require('@/core/name').isolateName;

async function search(query) {
  const mappings = {};
  const unresolved = new Set();
  let root;

  const processQueryResult = (qres, match = false) => {
    qres.rows.forEach(row => {
      const {id, name, parent_id} = row;
      const node = mappings[id] || {children: []};
      mappings[id] = node;
      if (node.id == null) {
        Object.assign(node, row, {
          name: isolateName(name),
          match
        });
      }

      if (parent_id == null) {
        root = node;
      } else {
        const parent = mappings[parent_id] || {children: []};
        mappings[parent_id] = parent;
        if (!parent.children.some(child => child.id === node.id)) parent.children.push(node);
        if (parent.id == null) unresolved.add(parent_id);
      }
    });
  };

  let qres = await db.query('SELECT DISTINCT id, name, size, parent_id FROM imagenet WHERE lower(substring(name from \'[^>]+$\')) LIKE \'%\' || $1 || \'%\'', [query.toLowerCase()]);
  processQueryResult(qres, true);
  while (unresolved.size > 0) {
    qres = await db.query(`SELECT id, name, size, parent_id FROM imagenet WHERE id IN (${Array.from(unresolved).join(',')})`);
    unresolved.clear();
    processQueryResult(qres);
  }

  return root ? [root] : [];
}

module.exports = {
  search
};
