const db = require('@/core/db');
const isolateName = require('@/core/name').isolateName;

function NodeNotFoundError() {}

function fetchNode(id) {
  let dbPromise;
  if (id == null) {
    dbPromise = db.query('SELECT id, name, size FROM imagenet WHERE parent_id IS NULL');
  } else {
    dbPromise = db.query('SELECT id, name, size FROM imagenet WHERE id = $1', [id]);
  }

  return dbPromise.then(qres => {
    if (qres.rows.length == 0) throw new NodeNotFoundError();
    return qres.rows[0];
  }).then(node => {
    return db.query('SELECT id, name, size FROM imagenet WHERE parent_id = $1', [node.id]).then(qres => {
      return Object.assign({}, node, {
        name: isolateName(node.name),
        children: qres.rows.map(child => Object.assign({}, child, {
          name: isolateName(child.name)
        }))
      });
    });
  });
}

module.exports = {
  fetchNode,
  NodeNotFoundError
};
