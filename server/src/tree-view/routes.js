const express = require('express');
const router = express.Router();
const fetchNode = require('./fetch-node').fetchNode;
const search = require('./search').search;
const NodeNotFoundError = require('./fetch-node').NodeNotFoundError;

router.get('/', (req, res) => {
  const query = req.query.query;
  if (query != null) {
    search(query).then(nodes => {
      res.json(nodes);
    }).catch(err => {
      console.error(err);
      res.status(500).send();
    });
  } else {
    fetchNode().then(node => {
      res.json(node);
    }).catch(err => {
      if (err instanceof NodeNotFoundError) {
        res.status(404).send();
      } else {
        res.status(500).send();
      }
    });
  }
});

router.get('/:id', (req, res) => {
  fetchNode(req.params.id).then(node => {
    res.json(node);
  }).catch(err => {
    if (err instanceof NodeNotFoundError) {
      res.status(404).send();
    } else {
      res.status(500).send();
    }
  });
});

module.exports = router;
