const pg = require('pg');
const Promise = require('promise');
require('rxjs'); // import all operators

// CREATE TABLE imagenet (
//   id        BIGINT PRIMARY KEY,
//   name      VARCHAR(500),
//   size      INTEGER,
//   parent_id BIGINT
// );

var config = {
  user: 'operam',
  database: 'operam_db',
  password: 'password',
  host: 'localhost',
  port: 5432,
  max: 10,
  idleTimeoutMillis: 30000
};

const pool = new pg.Pool(config);

pool.on('error', function (err) {
  console.error('idle client error', err.message, err.stack);
});

module.exports.query = async (text, values) => {
  return await new Promise((resolve, reject) => {
    pool.query(text, values, (err, res) => {
      if (err) {
        reject(err);
      } else {
        resolve(res);
      }
    });
  });
};

// module.exports.connect = function (callback) {
//   return pool.connect(callback);
// };
