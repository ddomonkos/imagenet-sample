const SEPARATOR = ' > ';

function isolateName(fullName) {
  const split = fullName.split(SEPARATOR);
  return split[split.length - 1];
}

module.exports = {
  SEPARATOR,
  isolateName
};
