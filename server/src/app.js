const express = require('express');
const app = express();
const cors = require('cors');
const treeView = require('./tree-view/routes');

app.listen(3000);

app.use(cors());
app.set('json spaces', 2);

app.use('/', treeView);
