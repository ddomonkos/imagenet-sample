const db = require('@/core/db');
const isolateName = require('@/core/name').isolateName;

function buildTree() {
  return db.query('SELECT id, name, size, parent_id FROM imagenet').then(({rows}) => {
    let root;
    const mappings = {};
    rows.forEach(({id, name, size, parent_id}) => {
      const node = mappings[id] || {children: []};
      mappings[id] = node;
      Object.assign(node, {id, size, name: isolateName(name)});

      if (parent_id == null) {
        if (root) throw `two or more roots found (${root.id}, ${node.id})`;
        root = node;
      } else {
        const parent = mappings[parent_id] || {children: []};
        mappings[parent_id] = parent;
        parent.children.push(node);
      }
    });
    return root;
  });
}

module.exports = buildTree;
