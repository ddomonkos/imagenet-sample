var path = require('path');

var projectRoot = path.resolve(__dirname, '..');
var resolve = (...args) => path.resolve(projectRoot, ...args); // shortcut

module.exports = {
  entry: {
    server: ['babel-polyfill', resolve('src/app.js')],
    scraper: ['babel-polyfill', resolve('src/scraper/job.js')],
    convertor: ['babel-polyfill', resolve('src/convertor/job.js')]
  },
  output: {
    filename: '[name].js',
    path: resolve('dist')
  },
  target: 'node',
  resolve: {
    alias: {
      '@': resolve('src')
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      }
    ]
  }
};
