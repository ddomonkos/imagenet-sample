# Prepare
Run **npm install** in both subprojects, **server** and **client**.


# Tasks 1 + 2

## Run
Go to the subproject **server** and run **npm run scraper**.

## Notes
Since there are some nodes that have the same name and are in the same category, apart from names we also need to store their IDs and IDs of their parents, if we want to preserve the hierarchy correctly.


# Task 3

## Run
Go to the subproject **server** and run **npm run convertor**.

## Notes
The script simply prints out the tree. Complexity is **O(n)**.


# Task 4

## Run
1. Go to subproject **server** and run **npm run server**.
2. In another window, go to subproject **client** and run **npm start**.

## Notes
For convenience, I used a skeleton project for the frontend that I regularly use (create-react-app). The setup assumes that both servers are run on the same machine (localhost) and ports 3000 and 3001 are free.

Things that I conciously left out to keep it simple:

* loading
* animations
* pagination

Also, since the example is simple, I avoided Redux as well.
